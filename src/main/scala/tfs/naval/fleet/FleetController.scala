package tfs.naval.fleet

import tfs.naval.model.{Fleet, Ship}

trait FleetController {

  // добавить корабль во флот
  def enrichFleet(fleet: Fleet, name: String, ship: Ship): Fleet

}
